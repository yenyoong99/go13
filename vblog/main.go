package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/go-course-project/go13/vblog/conf"
	"gitlab.com/go-course-project/go13/vblog/ioc"

	// 通过import方法 完成注册
	_ "gitlab.com/go-course-project/go13/vblog/apps"
)

func main() {
	// 1. 初始化程序配置, 这里没有配置，使用默认值
	if err := conf.LoadFromEnv(); err != nil {
		panic(err)
	}

	// 2. 程序对象管理
	if err := ioc.Init(); err != nil {
		panic(err)
	}

	// Protocol
	engine := gin.Default()

	rr := engine.Group("/vblog/api/v1")
	ioc.RegistryGinApi(rr)

	// 把Http协议服务器启动起来
	if err := engine.Run(":8080"); err != nil {
		panic(err)
	}
}
