package apps

// 注册业务实现: API + Controller
import (
	// 通过import方法 完成注册
	_ "gitlab.com/go-course-project/go13/vblog/apps/blog/api"
	_ "gitlab.com/go-course-project/go13/vblog/apps/blog/impl"
	_ "gitlab.com/go-course-project/go13/vblog/apps/token/api"
	_ "gitlab.com/go-course-project/go13/vblog/apps/token/impl"
	_ "gitlab.com/go-course-project/go13/vblog/apps/user/impl"
)
