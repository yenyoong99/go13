package user

// 什么是枚举？ 为什么要使用枚举?
// 枚举 把所有可选项 一一例举出来
// Role,   Admin/Member   Role string ==> "", 看代码的人 不知道 Role到了应该传怎么值
// 到底有哪些值 可传, 是需要程序列举出来
// 枚举核心能力: 约束 只能传递 列举出来的值, 其他的值都不允许传递

// 通过声明一种自定义类型来声明一种类型
type Role int

// 通过定义满足类型的常量，来定义满足这个类型的列表
// ROLE_MEMBER/ROLE_ADMIN
const (
	// 当 值为0的时候, 就是默认值
	// 枚举命名风格:
	ROLE_MEMBER Role = iota
	ROLE_ADMIN
)
