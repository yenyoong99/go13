package impl_test

import (
	"testing"

	"gitlab.com/go-course-project/go13/vblog/apps/blog"
	"gitlab.com/go-course-project/go13/vblog/common"
)

func TestCreateBlog(t *testing.T) {
	req := blog.NewCreateBlogRequest()
	req.Title = "go语言全栈开发"
	req.Author = "oldyu"
	req.Content = "xxx"
	req.Summary = "xx"
	req.Tags["目录"] = "Go语言"
	ins, err := impl.CreateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ins)
}

func TestQueryBlog(t *testing.T) {
	req := blog.NewQueryBlogRequest()
	set, err := impl.QueryBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestDescribeBlog(t *testing.T) {
	req := blog.NewDescribeBlogRequest("48")
	set, err := impl.DescribeBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

// req.Title = "go语言全栈开发"
// req.Author = "oldyu"
// req.Content = "xxx"
// req.Summary = "xx"
// req.Tags["目录"] = "Go语言"
func TestUpdateBlogPatchMod(t *testing.T) {
	req := blog.NewUpdateBlogRequest("48")
	req.UpdateMode = common.UPDATE_MODE_PATCH
	req.Title = "go语言全栈开发V2"
	set, err := impl.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestUpdateBlogPutMod(t *testing.T) {
	req := blog.NewUpdateBlogRequest("48")
	req.Title = "go语言全栈开发V3"
	req.Content = "v3"
	req.Author = "v3"
	set, err := impl.UpdateBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}

func TestDeleteBlog(t *testing.T) {
	req := blog.NewDeleteBlogRequest("47")
	set, err := impl.DeleteBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(set)
}
