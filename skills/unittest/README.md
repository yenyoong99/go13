# 单元测试

针对某个功能(函数) 来验证功能的正确性



## 如何编写单元测试

单元测试符合条件: 编写一个单元测试的函数来进行目标的函数的测试(unittest.Sum)
+ 单元测试的函数需要放到一个包里, 独立一个测试包, unittest_test.TestSum, 目标测试函数的名称TestSum
+ 默认规则: 单元测试包的包名称: {filename}_test(add_test.go) 命名, 同一个目录下吗，可以在独立存在这个包的测试包: unittest_test
    + 被测试的文件名称: xxx_test.go
    + 测试包的名称: xxxx_test (package unittest_test)
    + 函数签名: TestXXXX(t testing.T)

```go
func TestSum(t *testing.T) {
	t.Log(unittest.Sum(1, 2))
}
```


## 单元测试如何配置

1. vscode 单元测试读取环境变量的配置文件路径配置
```
{
    "go.testEnvFile": "${workspaceFolder}/etc/unit_test.env"
}
```

vs 配置 go test flag配置: go.testFlags
+ -count=1: (cached), 单元测试没有修改也想强制运行一次
+ -v: 单元测试内部想 大小信息 print, t.Log(), 需要加上该参数
```json
"go.testFlags": [
"-count=1",
"-v"
],
```

2. 在文件里吗补充需要注入的环境变量
```env
ARG1=1
ARG2=2
```

3. 单元测试里 使用该环境变量
```go
// 这个单元测试需要读取外部变量?
// 这里的ARG1/ARG2如果传递给单元测试
// run test: go test -timeout 30s -run ^TestSum$ gitlab.com/go-course-project/go13/skills/unittest
// 当我们点击 debug test时 怎么注入自定义环境变量
func TestSum(t *testing.T) {
	// read file, 很难统一路径问题: 文件有相对路由和绝对路径
	// 环境变量,
	a1 := os.Getenv("ARG1")
	a2 := os.Getenv("ARG2")
	a1I, _ := strconv.Atoi(a1)
	a2I, _ := strconv.Atoi(a2)
	t.Log(unittest.Sum(a1I, a2I))
}
```
